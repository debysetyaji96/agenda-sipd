<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;


class Event extends Model
{
    protected $table = "events";
    protected $primarykey = "id";



    use HasFactory;
    protected $fillable = [

        'title', 'start', 'end'

    ];
}
