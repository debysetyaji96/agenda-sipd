/*
 Navicat Premium Data Transfer

 Source Server         : kemendagri
 Source Server Type    : PostgreSQL
 Source Server Version : 110011
 Source Host           : localhost:5432
 Source Catalog        : agenda
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110011
 File Encoding         : 65001

 Date: 22/07/2021 13:34:53
*/


-- ----------------------------
-- Sequence structure for agenda_rapat_id_rapat_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."agenda_rapat_id_rapat_seq";
CREATE SEQUENCE "public"."agenda_rapat_id_rapat_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for arsip_rapat_id_arsip_rapat_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."arsip_rapat_id_arsip_rapat_seq";
CREATE SEQUENCE "public"."arsip_rapat_id_arsip_rapat_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for events_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."events_id_seq";
CREATE SEQUENCE "public"."events_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for migrations_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."migrations_id_seq";
CREATE SEQUENCE "public"."migrations_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for peserta_rapat_id_peserta_rapat_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."peserta_rapat_id_peserta_rapat_seq";
CREATE SEQUENCE "public"."peserta_rapat_id_peserta_rapat_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for pimpinan_rapat_id_pimpinan_rapat_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."pimpinan_rapat_id_pimpinan_rapat_seq";
CREATE SEQUENCE "public"."pimpinan_rapat_id_pimpinan_rapat_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for surat_rapat_id_surat_rapat_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."surat_rapat_id_surat_rapat_seq";
CREATE SEQUENCE "public"."surat_rapat_id_surat_rapat_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for users_id_user_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_user_seq";
CREATE SEQUENCE "public"."users_id_user_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Table structure for agenda_rapat
-- ----------------------------
DROP TABLE IF EXISTS "public"."agenda_rapat";
CREATE TABLE "public"."agenda_rapat" (
  "id_rapat" int8 NOT NULL DEFAULT nextval('agenda_rapat_id_rapat_seq'::regclass),
  "rapat_id_pimpinan_rapat" int8 NOT NULL,
  "rapat_id_nomor_surat" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "perihal_rapat" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "jadwal_rapat" date NOT NULL,
  "waktu_rapat" time(0) NOT NULL,
  "ruangan_rapat" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "disposisi" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "keterangan" text COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of agenda_rapat
-- ----------------------------

-- ----------------------------
-- Table structure for arsip_rapat
-- ----------------------------
DROP TABLE IF EXISTS "public"."arsip_rapat";
CREATE TABLE "public"."arsip_rapat" (
  "id_arsip_rapat" int8 NOT NULL DEFAULT nextval('arsip_rapat_id_arsip_rapat_seq'::regclass),
  "arsip_rapat_id_agenda_rapat" int8 NOT NULL,
  "hasil_rapat" text COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of arsip_rapat
-- ----------------------------

-- ----------------------------
-- Table structure for events
-- ----------------------------
DROP TABLE IF EXISTS "public"."events";
CREATE TABLE "public"."events" (
  "id" int8 NOT NULL DEFAULT nextval('events_id_seq'::regclass),
  "title" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "start" date NOT NULL,
  "end" date NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of events
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS "public"."migrations";
CREATE TABLE "public"."migrations" (
  "id" int4 NOT NULL DEFAULT nextval('migrations_id_seq'::regclass),
  "migration" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "batch" int4 NOT NULL
)
;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO "public"."migrations" VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO "public"."migrations" VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO "public"."migrations" VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO "public"."migrations" VALUES (4, '2020_09_17_090037_create_surat_rapat_table', 1);
INSERT INTO "public"."migrations" VALUES (5, '2020_09_17_090701_create_pimpinan_rapat_table', 1);
INSERT INTO "public"."migrations" VALUES (6, '2020_09_17_091017_create_agenda_rapat_table', 1);
INSERT INTO "public"."migrations" VALUES (7, '2020_09_18_090038_create_peserta_rapat_table', 1);
INSERT INTO "public"."migrations" VALUES (8, '2020_10_19_032646_create_arsip_rapat', 1);
INSERT INTO "public"."migrations" VALUES (9, '2021_06_22_062101_create_events_table', 2);

-- ----------------------------
-- Table structure for peserta_rapat
-- ----------------------------
DROP TABLE IF EXISTS "public"."peserta_rapat";
CREATE TABLE "public"."peserta_rapat" (
  "id_peserta_rapat" int8 NOT NULL DEFAULT nextval('peserta_rapat_id_peserta_rapat_seq'::regclass),
  "peserta_rapat_id_rapat" int8 NOT NULL,
  "nama_peserta" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "jabatan_peserta" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "jenis_kelamin" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of peserta_rapat
-- ----------------------------

-- ----------------------------
-- Table structure for pimpinan_rapat
-- ----------------------------
DROP TABLE IF EXISTS "public"."pimpinan_rapat";
CREATE TABLE "public"."pimpinan_rapat" (
  "id_pimpinan_rapat" int8 NOT NULL DEFAULT nextval('pimpinan_rapat_id_pimpinan_rapat_seq'::regclass),
  "nama_pimpinan" varchar(60) COLLATE "pg_catalog"."default" NOT NULL,
  "username" varchar(30) COLLATE "pg_catalog"."default" NOT NULL,
  "nip" varchar(70) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "jabatan" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "jenis_kelamin" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "no_telepon" varchar(17) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of pimpinan_rapat
-- ----------------------------

-- ----------------------------
-- Table structure for surat_rapat
-- ----------------------------
DROP TABLE IF EXISTS "public"."surat_rapat";
CREATE TABLE "public"."surat_rapat" (
  "id_surat_rapat" int8 NOT NULL DEFAULT nextval('surat_rapat_id_surat_rapat_seq'::regclass),
  "id_nomor_surat" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "perihal_surat" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "tujuan_surat" varchar(120) COLLATE "pg_catalog"."default" NOT NULL,
  "isi_surat" text COLLATE "pg_catalog"."default" NOT NULL,
  "jabatan_pengirim" varchar(70) COLLATE "pg_catalog"."default" NOT NULL,
  "pengirim_surat" varchar(70) COLLATE "pg_catalog"."default" NOT NULL,
  "tanggal_surat" date NOT NULL,
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of surat_rapat
-- ----------------------------
INSERT INTO "public"."surat_rapat" VALUES (1, 'kemendagri/137/TGS/2021/062842', 'Melelangkan Barang', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Tamawijaya Abimanyu', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (2, 'kemendagri/255/TGS/2021/062842', 'Penataran kebahasan', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Harini Sasmaya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (3, 'kemendagri/661/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Guntur Galih', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (4, 'kemendagri/205/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Gantari Narasnama', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (5, 'kemendagri/555/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Seluruh karyawan PT. Wijaya Kusuma Wijaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Dimas Janu', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (6, 'kemendagri/512/TGS/2021/062842', 'Penataran kebahasan', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Rosa Anjani', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (7, 'kemendagri/668/TGS/2021/062842', 'Melelangkan Barang', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Adipramana Satya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (8, 'kemendagri/500/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Tisna Paramastri', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (9, 'kemendagri/648/TGS/2021/062842', 'Undangan Rapat', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Adinata Hanafi', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (10, 'kemendagri/153/TGS/2021/062842', 'Melelangkan Barang', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Cahaya Dianti', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (11, 'kemendagri/998/TGS/2021/062842', 'Permohonan Izin Mengajar', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Birawa Wirya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (12, 'kemendagri/432/TGS/2021/062842', 'Pengiriman Daftar Buku', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Harini Harini', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (13, 'kemendagri/361/TGS/2021/062842', 'Permohonan Studi Banding', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Agnibrata Damar', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (14, 'kemendagri/265/TGS/2021/062842', 'Undangan Rapat', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Listia Kani', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (15, 'kemendagri/451/TGS/2021/062842', 'Permohonan Kenaikan Kelas', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Widodo Ganesh', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (16, 'kemendagri/813/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Anindya Laya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (17, 'kemendagri/929/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Seluruh karyawan PT. Wijaya Kusuma Wijaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Bhanu Pranawa', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (18, 'kemendagri/787/TGS/2021/062842', 'Penataran kebahasan', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Yetri Radmila', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (188, 'kemendagri/924/TGS/2021/062842', 'Undangan Rapat', 'Ketua Panitia Lomba', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Intan Tri', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (19, 'kemendagri/697/TGS/2021/062842', 'Pengiriman Daftar Buku', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Abimana Jenaka', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (20, 'kemendagri/946/TGS/2021/062842', 'Penataran kebahasan', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Dwi Laksita', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (21, 'kemendagri/626/TGS/2021/062842', 'Penataran kebahasan', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Langit Lasmana', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (22, 'kemendagri/126/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Seluruh karyawan PT. Wijaya Kusuma Wijaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Putri Mahawirya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (23, 'kemendagri/331/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Sigit Galih', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (24, 'kemendagri/383/TGS/2021/062842', 'Permohonan Izin Mengajar', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Diajeng Pramata', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (25, 'kemendagri/684/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Widayaka Herdian', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (26, 'kemendagri/732/TGS/2021/062842', 'Pengusulan calon pegawai', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Ina Kila', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (27, 'kemendagri/485/TGS/2021/062842', 'Undangan Rapat', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Halim Mahapraja', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (28, 'kemendagri/721/TGS/2021/062842', 'Pengiriman Daftar Buku', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Airani Gemintang', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (29, 'kemendagri/506/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Tamawijaya Mahajana', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (30, 'kemendagri/180/TGS/2021/062842', 'Panggilan kerja', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Intan Keinan', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (31, 'kemendagri/565/TGS/2021/062842', 'Undangan Rapat', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Sudiro Dharma', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (32, 'kemendagri/689/TGS/2021/062842', 'Panggilan kerja', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Ina Lalita', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (33, 'kemendagri/148/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Pramudya Kawindra', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (34, 'kemendagri/539/TGS/2021/062842', 'Permohonan Izin Mengajar', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Permata Indira', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (35, 'kemendagri/676/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Satriya Bayu', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (36, 'kemendagri/201/TGS/2021/062842', 'Pengiriman rak buku', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Ayu Septha', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (37, 'kemendagri/805/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Ketua Panitia Lomba', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Endaru Indra', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (38, 'kemendagri/357/TGS/2021/062842', 'Permohonan Izin Mengajar', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Lika Dhatu', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (39, 'kemendagri/975/TGS/2021/062842', 'Undangan Rapat', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Hartanto Hery', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (40, 'kemendagri/714/TGS/2021/062842', 'Panggilan kerja', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Seta Rahmi', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (41, 'kemendagri/465/TGS/2021/062842', 'Pengusulan calon pegawai', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Bramantya Pradigta', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (42, 'kemendagri/521/TGS/2021/062842', 'Permohonan Izin Mengajar', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Tri Madaharsa', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (43, 'kemendagri/806/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Abimanyu Nugraha', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (44, 'kemendagri/751/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Rahmi Laksita', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (45, 'kemendagri/643/TGS/2021/062842', 'Panggilan kerja', 'Seluruh karyawan PT. Wijaya Kusuma Wijaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Santoso Arjanta', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (47, 'kemendagri/234/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Naufal Dipa', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (48, 'kemendagri/828/TGS/2021/062842', 'Panggilan kerja', 'Orang tua / Wali Murid Kelas XI , XII, XII SMA Bakti Mulya Jakarta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Rajini Tanya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (49, 'kemendagri/161/TGS/2021/062842', 'Undangan Rapat Panitia Kemendagri', 'Seluruh karyawan PT. Wijaya Kusuma Wijaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Raharja Pramuditha', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (50, 'kemendagri/119/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Manika JuWIB', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (51, 'kemendagri/584/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Widayaka Danurdara', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (52, 'kemendagri/394/TGS/2021/062842', 'Permohonan Izin Mengajar', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Jayanti Parmadita', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (53, 'kemendagri/401/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Lakeswara Sigit', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (54, 'kemendagri/243/TGS/2021/062842', 'Penataran kebahasan', 'Orang tua / Wali Murid Kelas XI , XII, XII SMA Bakti Mulya Jakarta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Karina Ratimaya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (55, 'kemendagri/130/TGS/2021/062842', 'Undangan Rapat Panitia Kemendagri', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Nusantara Bajradaka', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (56, 'kemendagri/621/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Mentari Widhiani', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (57, 'kemendagri/193/TGS/2021/062842', 'Penataran kebahasan', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Indra Rezvan', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (58, 'kemendagri/656/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Wening Ratimaya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (59, 'kemendagri/109/TGS/2021/062842', 'Pengiriman Daftar Buku', 'Seluruh karyawan PT. Wijaya Kusuma Wijaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Aswangga Kawindra', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (60, 'kemendagri/896/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Lukita Wuri', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (61, 'kemendagri/620/TGS/2021/062842', 'Permohonan Studi Banding', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Gentala Prasetyo', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (62, 'kemendagri/630/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Isty Ayu', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (63, 'kemendagri/220/TGS/2021/062842', 'Panggilan kerja', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Kawindra Hery', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (64, 'kemendagri/184/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Laya Nala', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (65, 'kemendagri/574/TGS/2021/062842', 'Pengusulan calon pegawai', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Daniswara Santoso', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (66, 'kemendagri/746/TGS/2021/062842', 'Undangan Rapat Panitia Kemendagri', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Gadis Ratu', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (67, 'kemendagri/509/TGS/2021/062842', 'Pengusulan calon pegawai', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Edi Mahapraja', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (68, 'kemendagri/904/TGS/2021/062842', 'Melelangkan Barang', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Yulianti Bintang', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (69, 'kemendagri/681/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Danurdara Ihsan', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (70, 'kemendagri/195/TGS/2021/062842', 'Pengusulan calon pegawai', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Mustika Pratista', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (71, 'kemendagri/964/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Teja Rahardian', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (72, 'kemendagri/116/TGS/2021/062842', 'Pengiriman Daftar Buku', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Estiana Lestia', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (73, 'kemendagri/769/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Hery Gunawan', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (75, 'kemendagri/918/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Daniswara Karunia', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (76, 'kemendagri/986/TGS/2021/062842', 'Penataran kebahasan', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Cyntia Candramaya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (77, 'kemendagri/535/TGS/2021/062842', 'Undangan Rapat', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Tohpati Yudayana', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (80, 'kemendagri/236/TGS/2021/062842', 'Permohonan Izin Mengajar', 'Ketua Panitia Lomba', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Indri Paramita', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (82, 'kemendagri/774/TGS/2021/062842', 'Undangan Rapat', 'Orang tua / Wali Murid Kelas XI , XII, XII SMA Bakti Mulya Jakarta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Isthika Asri', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (83, 'kemendagri/316/TGS/2021/062842', 'Permohonan Kenaikan Kelas', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Santoso Ganendra', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (84, 'kemendagri/124/TGS/2021/062842', 'Pengiriman rak buku', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Permata Intan', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (85, 'kemendagri/143/TGS/2021/062842', 'Permohonan Kenaikan Kelas', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Dharma Ardiyanto', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (86, 'kemendagri/463/TGS/2021/062842', 'Permohonan Kenaikan Kelas', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Anindya Lanita', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (87, 'kemendagri/486/TGS/2021/062842', 'Undangan Rapat Panitia Kemendagri', 'Ketua Panitia Lomba', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Budiono Herdian', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (88, 'kemendagri/412/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Seluruh karyawan PT. Wijaya Kusuma Wijaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Ratri Wiyana', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (89, 'kemendagri/427/TGS/2021/062842', 'Melelangkan Barang', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Rangga Ihsan', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (91, 'kemendagri/658/TGS/2021/062842', 'Permohonan tenaga kerja', 'Orang tua / Wali Murid Kelas XI , XII, XII SMA Bakti Mulya Jakarta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Susilo Ganesh', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (92, 'kemendagri/675/TGS/2021/062842', 'Melelangkan Barang', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Pertiwi Laksita', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (93, 'kemendagri/991/TGS/2021/062842', 'Undangan Rapat', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Praba Jenaka', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (94, 'kemendagri/747/TGS/2021/062842', 'Permohonan Izin Mengajar', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Basagita Mahawirya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (95, 'kemendagri/163/TGS/2021/062842', 'Undangan Rapat Panitia Kemendagri', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Agung Jumanta', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (96, 'kemendagri/887/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Dianti Asri', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (97, 'kemendagri/459/TGS/2021/062842', 'Permohonan Studi Banding', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Perdana Estu', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (98, 'kemendagri/554/TGS/2021/062842', 'Undangan Rapat Panitia Kemendagri', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Pratista Ratna', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (99, 'kemendagri/297/TGS/2021/062842', 'Permohonan tenaga kerja', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Reswara Ulung', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (100, 'kemendagri/407/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Sasmaya Kasyaira', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (101, 'kemendagri/540/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Raditya Bagaskoro', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (102, 'kemendagri/103/TGS/2021/062842', 'Panggilan kerja', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Pertiwi Tisna', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (103, 'kemendagri/952/TGS/2021/062842', 'Melelangkan Barang', 'Ketua Panitia Lomba', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Harjita Endang', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (104, 'kemendagri/984/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Intan Nidya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (105, 'kemendagri/822/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Aradhana Pramuditha', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (107, 'kemendagri/943/TGS/2021/062842', 'Permohonan Studi Banding', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Hapsari Dharma', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (108, 'kemendagri/113/TGS/2021/062842', 'Pengusulan calon pegawai', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Wikasita Lukita', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (109, 'kemendagri/435/TGS/2021/062842', 'Permohonan Kenaikan Kelas', 'Ketua Panitia Lomba', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Endaru Gunawan', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (110, 'kemendagri/917/TGS/2021/062842', 'Permohonan Izin Mengajar', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Lituhayu Yuni', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (111, 'kemendagri/654/TGS/2021/062842', 'Penataran kebahasan', 'Ketua Panitia Lomba', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Bayu Ardiyanto', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (112, 'kemendagri/150/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Kasyaira Mahawirya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (113, 'kemendagri/916/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Ketua Panitia Lomba', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Adipramana Hamdan', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (114, 'kemendagri/837/TGS/2021/062842', 'Pengiriman rak buku', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Madana Gyandra', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (118, 'kemendagri/141/TGS/2021/062842', 'Pengusulan calon pegawai', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Hayu Laksmi', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (119, 'kemendagri/934/TGS/2021/062842', 'Permohonan Studi Banding', 'Orang tua / Wali Murid Kelas XI , XII, XII SMA Bakti Mulya Jakarta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Respati Mahapraja', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (120, 'kemendagri/172/TGS/2021/062842', 'Permohonan Studi Banding', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Lasmaya Jelita', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (121, 'kemendagri/169/TGS/2021/062842', 'Permohonan Kenaikan Kelas', 'Orang tua / Wali Murid Kelas XI , XII, XII SMA Bakti Mulya Jakarta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Rezvan Kamajaya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (122, 'kemendagri/292/TGS/2021/062842', 'Penataran kebahasan', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Cahaya Permata', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (123, 'kemendagri/894/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Jumanta Herjuno', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (124, 'kemendagri/471/TGS/2021/062842', 'Permohonan Studi Banding', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Bratarini Widi', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (125, 'kemendagri/601/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Pradnyana Leksana', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (126, 'kemendagri/547/TGS/2021/062842', 'Undangan Rapat Panitia Kemendagri', 'Orang tua / Wali Murid Kelas XI , XII, XII SMA Bakti Mulya Jakarta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Manda Mahadewi', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (127, 'kemendagri/862/TGS/2021/062842', 'Undangan Rapat', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Tegar Hendro', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (128, 'kemendagri/306/TGS/2021/062842', 'Permohonan Kenaikan Kelas', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Tarasari Yuni', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (129, 'kemendagri/542/TGS/2021/062842', 'Penataran kebahasan', 'Ketua Panitia Lomba', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Cakrawala Rahardian', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (130, 'kemendagri/100/TGS/2021/062842', 'Permohonan Kenaikan Kelas', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Rani April', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (131, 'kemendagri/440/TGS/2021/062842', 'Permohonan Studi Banding', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Guritno Gilang', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (132, 'kemendagri/679/TGS/2021/062842', 'Undangan Rapat Panitia Kemendagri', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Arsyana Putri', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (133, 'kemendagri/293/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Rama Kamandaka', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (134, 'kemendagri/464/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Dianti Wiyana', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (135, 'kemendagri/736/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Rahardian Ihsan', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (136, 'kemendagri/872/TGS/2021/062842', 'Penataran kebahasan', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Banurasmi Aruna', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (137, 'kemendagri/580/TGS/2021/062842', 'Pengiriman rak buku', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Dewangga Estu', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (138, 'kemendagri/395/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Diatmika Pramidita', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (139, 'kemendagri/958/TGS/2021/062842', 'Permohonan Studi Banding', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Raharja Dharma', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (140, 'kemendagri/288/TGS/2021/062842', 'Pengiriman Daftar Buku', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Saraswati Manika', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (141, 'kemendagri/198/TGS/2021/062842', 'Undangan Rapat', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Nareswara Surya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (144, 'kemendagri/982/TGS/2021/062842', 'Panggilan kerja', 'Seluruh karyawan PT. Wijaya Kusuma Wijaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Nirmala Ndari', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (145, 'kemendagri/704/TGS/2021/062842', 'Undangan Rapat', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Widayaka Edi', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (146, 'kemendagri/888/TGS/2021/062842', 'Pengusulan calon pegawai', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Rahmi Tri', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (147, 'kemendagri/529/TGS/2021/062842', 'Permohonan Studi Banding', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Widodo Ismoyono', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (149, 'kemendagri/883/TGS/2021/062842', 'Undangan Rapat', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Agam Reza ', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (150, 'kemendagri/385/TGS/2021/062842', 'Undangan Rapat', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Wuri Nawa', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (151, 'kemendagri/583/TGS/2021/062842', 'Melelangkan Barang', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Jaya Dimas', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (152, 'kemendagri/206/TGS/2021/062842', 'Pengiriman Daftar Buku', 'Orang tua / Wali Murid Kelas XI , XII, XII SMA Bakti Mulya Jakarta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Tyas Madarsana', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (153, 'kemendagri/716/TGS/2021/062842', 'Permohonan Kenaikan Kelas', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Kawindra Hamdan', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (154, 'kemendagri/429/TGS/2021/062842', 'Undangan Rapat Panitia Kemendagri', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Gemintang Bandiani', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (155, 'kemendagri/705/TGS/2021/062842', 'Panggilan kerja', 'Orang tua / Wali Murid Kelas XI , XII, XII SMA Bakti Mulya Jakarta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Bagaskoro Adiwangsa', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (156, 'kemendagri/390/TGS/2021/062842', 'Undangan Rapat Panitia Kemendagri', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Puspa Madarsana', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (157, 'kemendagri/493/TGS/2021/062842', 'Permohonan Izin Mengajar', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Arga Reza ', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (158, 'kemendagri/748/TGS/2021/062842', 'Melelangkan Barang', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Pratista Pawana', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (159, 'kemendagri/170/TGS/2021/062842', 'Melelangkan Barang', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Pranadipa Susilo', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (160, 'kemendagri/333/TGS/2021/062842', 'Melelangkan Barang', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Candramaya Anatari', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (162, 'kemendagri/422/TGS/2021/062842', 'Permohonan tenaga kerja', 'Ketua Panitia Lomba', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Manda Keinan', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (163, 'kemendagri/709/TGS/2021/062842', 'Permohonan tenaga kerja', 'Bapak Penatua', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Gadhing Bhanu', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (165, 'kemendagri/616/TGS/2021/062842', 'Pengusulan calon pegawai', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Bambang Mulyadi', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (168, 'kemendagri/256/TGS/2021/062842', 'Permohonan Kenaikan Kelas', 'Orang tua / Wali Murid Kelas XI , XII, XII SMA Bakti Mulya Jakarta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Madaharsa Saraswati', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (169, 'kemendagri/829/TGS/2021/062842', 'Permohonan Izin Mengajar', 'Orang tua / Wali Murid Kelas XI , XII, XII SMA Bakti Mulya Jakarta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Wirya Gardara', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (170, 'kemendagri/343/TGS/2021/062842', 'Penataran kebahasan', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Diajeng Hasya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (171, 'kemendagri/908/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'CEO PT. Amarta Jaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Pradigta Manggala', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (172, 'kemendagri/863/TGS/2021/062842', 'Memohon Izin Menggunakan Auditorium', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Airani Nala', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (173, 'kemendagri/925/TGS/2021/062842', 'Penataran kebahasan', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Cakra Birawa', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (174, 'kemendagri/627/TGS/2021/062842', 'Pengiriman rak buku', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Narasnama Maharani', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (175, 'kemendagri/366/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Seluruh karyawan PT. Wijaya Kusuma Wijaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'An.CAMAT PEKANSARI', 'Endang Tamawijaya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (176, 'kemendagri/404/TGS/2021/062842', 'Pengiriman Daftar Buku', 'Seluruh karyawan PT. Wijaya Kusuma Wijaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Bandiani Gemani', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (177, 'kemendagri/480/TGS/2021/062842', 'Pengiriman Daftar Buku', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Jatmika Guntur', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (180, 'kemendagri/160/TGS/2021/062842', 'Undangan Rapat', 'Seluruh karyawan PT. Wijaya Kusuma Wijaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Sekolah', 'Jayanti Sakya', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (181, 'kemendagri/443/TGS/2021/062842', 'Permohonan Studi Banding', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Ganesh Rahardian', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (182, 'kemendagri/865/TGS/2021/062842', 'Pengusulan calon pegawai', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Indriaya Cyntia', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (183, 'kemendagri/269/TGS/2021/062842', 'Permohonan Studi Banding', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Tohpati Yudayana', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (184, 'kemendagri/537/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Sasi Melati', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (185, 'kemendagri/819/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Orang tua / Wali Murid Kelas XI , XII, XII SMA Bakti Mulya Jakarta', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Gentala Widodo', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (187, 'kemendagri/590/TGS/2021/062842', 'Permohonan tenaga kerja', 'Pimpinan PT. Angkasa Raya Jl. Ahmad Yani No 879 Bengkulu', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Aunila', 'Ricky Gusti', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (190, 'kemendagri/776/TGS/2021/062842', 'Pengiriman rak buku', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Mirah Sukma', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (191, 'kemendagri/810/TGS/2021/062842', 'Pengiriman rak buku', 'Bapak/Ibu Orang tua/Wali murid', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Kepala Dinas Pertamanan dan Tata Kota Pemerintah Kota Pekalongan', 'Handaru Haribawa', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (192, 'kemendagri/848/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Seluruh karyawan PT. Wijaya Kusuma Wijaya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Graha Medika', 'Bandiani Nehan', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (193, 'kemendagri/994/TGS/2021/062842', 'Pengiriman rak buku', 'Dinas Pertamanan dan Tata Kota Kota Pekalongan', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Gardara Cakrawala', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (195, 'kemendagri/340/TGS/2021/062842', 'Melelangkan Barang', 'Bapak Presiden Beserta Jajarannya', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Pimpinan PT. Makmur Jaya Abadi', 'Agam Randika', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (196, 'kemendagri/973/TGS/2021/062842', 'Pengusulan kenaikan tingkat', 'Seluruh Guru Kelas XII SMA Negeri 1 Kota Tegal', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Ketua OSIS SMP Negeri 2 Pekanbaru', 'Nara Wangi', '2021-06-22', NULL, NULL);
INSERT INTO "public"."surat_rapat" VALUES (199, 'kemendagri/760/TGS/2021/062842', 'Pengusulan pengangkatan sekretaris', 'Kepala Dinas Pendidikan Kota Semarang', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A aperiam cum deserunt dignissimos ducimus error est exercitationem, nam neque. Ad aspernatur at delectus distinctio dolorum, exercitationem necessitatibus nulla quos tenetur!', 'Direktur Utama PT. Surya Kencana', 'Widura Budiono', '2021-06-22', NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id_user" int8 NOT NULL DEFAULT nextval('users_id_user_seq'::regclass),
  "username" varchar(35) COLLATE "pg_catalog"."default" NOT NULL,
  "nip" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "role" varchar(20) COLLATE "pg_catalog"."default" NOT NULL DEFAULT 'admin'::character varying,
  "remember_token" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(0),
  "updated_at" timestamp(0)
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (2, 'Superadmin Agenda', '111111111111', '$2y$10$wloTCoexwCVZ3IRTt8ImDuiWt7nwFQ7AaTh7nMdWiMukgAQFpwuQ6', 'superadmin', 'DtRqYu1XDjioPQg6PNuq9h17dtXGFAsdBNZi64mYZyDSPfMbgOPDnlxCLrmR', '2021-06-22 06:28:36', '2021-06-22 06:28:36');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."agenda_rapat_id_rapat_seq"
OWNED BY "public"."agenda_rapat"."id_rapat";
SELECT setval('"public"."agenda_rapat_id_rapat_seq"', 3, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."arsip_rapat_id_arsip_rapat_seq"
OWNED BY "public"."arsip_rapat"."id_arsip_rapat";
SELECT setval('"public"."arsip_rapat_id_arsip_rapat_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."events_id_seq"
OWNED BY "public"."events"."id";
SELECT setval('"public"."events_id_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."migrations_id_seq"
OWNED BY "public"."migrations"."id";
SELECT setval('"public"."migrations_id_seq"', 10, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."peserta_rapat_id_peserta_rapat_seq"
OWNED BY "public"."peserta_rapat"."id_peserta_rapat";
SELECT setval('"public"."peserta_rapat_id_peserta_rapat_seq"', 2, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."pimpinan_rapat_id_pimpinan_rapat_seq"
OWNED BY "public"."pimpinan_rapat"."id_pimpinan_rapat";
SELECT setval('"public"."pimpinan_rapat_id_pimpinan_rapat_seq"', 101, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."surat_rapat_id_surat_rapat_seq"
OWNED BY "public"."surat_rapat"."id_surat_rapat";
SELECT setval('"public"."surat_rapat_id_surat_rapat_seq"', 201, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."users_id_user_seq"
OWNED BY "public"."users"."id_user";
SELECT setval('"public"."users_id_user_seq"', 6, true);

-- ----------------------------
-- Indexes structure for table agenda_rapat
-- ----------------------------
CREATE INDEX "agenda_rapat_rapat_id_nomor_surat_index" ON "public"."agenda_rapat" USING btree (
  "rapat_id_nomor_surat" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "agenda_rapat_rapat_id_pimpinan_rapat_index" ON "public"."agenda_rapat" USING btree (
  "rapat_id_pimpinan_rapat" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table agenda_rapat
-- ----------------------------
ALTER TABLE "public"."agenda_rapat" ADD CONSTRAINT "agenda_rapat_pkey" PRIMARY KEY ("id_rapat");

-- ----------------------------
-- Indexes structure for table arsip_rapat
-- ----------------------------
CREATE INDEX "arsip_rapat_arsip_rapat_id_agenda_rapat_index" ON "public"."arsip_rapat" USING btree (
  "arsip_rapat_id_agenda_rapat" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table arsip_rapat
-- ----------------------------
ALTER TABLE "public"."arsip_rapat" ADD CONSTRAINT "arsip_rapat_pkey" PRIMARY KEY ("id_arsip_rapat");

-- ----------------------------
-- Primary Key structure for table events
-- ----------------------------
ALTER TABLE "public"."events" ADD CONSTRAINT "events_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table migrations
-- ----------------------------
ALTER TABLE "public"."migrations" ADD CONSTRAINT "migrations_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table peserta_rapat
-- ----------------------------
CREATE INDEX "peserta_rapat_peserta_rapat_id_rapat_index" ON "public"."peserta_rapat" USING btree (
  "peserta_rapat_id_rapat" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- ----------------------------
-- Checks structure for table peserta_rapat
-- ----------------------------
ALTER TABLE "public"."peserta_rapat" ADD CONSTRAINT "peserta_rapat_jenis_kelamin_check" CHECK (((jenis_kelamin)::text = ANY ((ARRAY['Laki - laki'::character varying, 'Perempuan'::character varying])::text[])));

-- ----------------------------
-- Primary Key structure for table peserta_rapat
-- ----------------------------
ALTER TABLE "public"."peserta_rapat" ADD CONSTRAINT "peserta_rapat_pkey" PRIMARY KEY ("id_peserta_rapat");

-- ----------------------------
-- Indexes structure for table pimpinan_rapat
-- ----------------------------
CREATE INDEX "pimpinan_rapat_no_telepon_index" ON "public"."pimpinan_rapat" USING btree (
  "no_telepon" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Uniques structure for table pimpinan_rapat
-- ----------------------------
ALTER TABLE "public"."pimpinan_rapat" ADD CONSTRAINT "pimpinan_rapat_no_telepon_unique" UNIQUE ("no_telepon");

-- ----------------------------
-- Checks structure for table pimpinan_rapat
-- ----------------------------
ALTER TABLE "public"."pimpinan_rapat" ADD CONSTRAINT "pimpinan_rapat_jenis_kelamin_check" CHECK (((jenis_kelamin)::text = ANY ((ARRAY['Laki - laki'::character varying, 'Perempuan'::character varying])::text[])));

-- ----------------------------
-- Primary Key structure for table pimpinan_rapat
-- ----------------------------
ALTER TABLE "public"."pimpinan_rapat" ADD CONSTRAINT "pimpinan_rapat_pkey" PRIMARY KEY ("id_pimpinan_rapat");

-- ----------------------------
-- Uniques structure for table surat_rapat
-- ----------------------------
ALTER TABLE "public"."surat_rapat" ADD CONSTRAINT "surat_rapat_id_nomor_surat_unique" UNIQUE ("id_nomor_surat");

-- ----------------------------
-- Primary Key structure for table surat_rapat
-- ----------------------------
ALTER TABLE "public"."surat_rapat" ADD CONSTRAINT "surat_rapat_pkey" PRIMARY KEY ("id_surat_rapat");

-- ----------------------------
-- Uniques structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_nip_unique" UNIQUE ("nip");

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id_user");

-- ----------------------------
-- Foreign Keys structure for table agenda_rapat
-- ----------------------------
ALTER TABLE "public"."agenda_rapat" ADD CONSTRAINT "agenda_rapat_rapat_id_nomor_surat_foreign" FOREIGN KEY ("rapat_id_nomor_surat") REFERENCES "public"."surat_rapat" ("id_nomor_surat") ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE "public"."agenda_rapat" ADD CONSTRAINT "agenda_rapat_rapat_id_pimpinan_rapat_foreign" FOREIGN KEY ("rapat_id_pimpinan_rapat") REFERENCES "public"."pimpinan_rapat" ("id_pimpinan_rapat") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table arsip_rapat
-- ----------------------------
ALTER TABLE "public"."arsip_rapat" ADD CONSTRAINT "arsip_rapat_arsip_rapat_id_agenda_rapat_foreign" FOREIGN KEY ("arsip_rapat_id_agenda_rapat") REFERENCES "public"."agenda_rapat" ("id_rapat") ON DELETE CASCADE ON UPDATE CASCADE;

-- ----------------------------
-- Foreign Keys structure for table peserta_rapat
-- ----------------------------
ALTER TABLE "public"."peserta_rapat" ADD CONSTRAINT "peserta_rapat_peserta_rapat_id_rapat_foreign" FOREIGN KEY ("peserta_rapat_id_rapat") REFERENCES "public"."agenda_rapat" ("id_rapat") ON DELETE CASCADE ON UPDATE CASCADE;
