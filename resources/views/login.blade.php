<!DOCTYPE html>
<html>

<head>
    <meta name="SIAPIN | Kemendagri (Sistem Informasi Rapat Pusdatin" content="{{ csrf_token() }}">
    <title>SIAPIN | Kemendagri (Sistem Informasi Rapat Pimpinan Pusdatin)</title>
    <link rel="stylesheet" href="{{ url('/css/root.css') }}">
    <link rel="stylesheet" href="{{ url('/css/style.css') }}">
    <link rel="stylesheet" href="{{ url('/css/page/login/login.css') }}">
</head>

<body>
    <div class="login-page-background">
        <div class="login-container">
            <form class="login-form" method="post" action="/api/login">
                @csrf
                <img id="login-logo" src="{{ url('/img/kemendagri.png') }}" alt="Logo">
                <h2>Login SIAPIN Kemendagri</h2>
                <label>Nip</label>
                <input id="email-input" name="nip" value="{{ old('nip') }}" type="text" placeholder="Masukan Sesuai Nip" />
                <br /><br />
                <label>Password</label>
                <input id="password-input" name="password" value="{{ old('password') }}" type="password" placeholder="*****" /> <br /><br />
                <div class="display">
                    <div class="left-display">
                        <input name="pimpinan" type="checkbox" />
                        <span>Login pimpinan</span>
                    </div>
                </div>
                @if($errors->has('nip'))
                <span class="error-message">{{ $errors->first('nip') }}</span>
                @elseif($errors->has('password'))
                <span class="error-message">{{ $errors->first('password') }}</span>
                @endif
                <button id="login-button" type="submit" class="button-primary">
                    Login
                </button>
            </form>
        </div>
    </div>
</body>

</html>
